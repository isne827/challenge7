#include <iostream>
#include "strvar.hpp"

int main()
{
    using namespace std;
    using namespace strvarken;
    StringVar string1, string2;
    char check, set;
    int located, begin, length;

    cout << "Enter 1st string : "; //collect first string1
    cin >> string1;
    cout << "Enetr 2nd string : "; //collect second string1
    cin >> string2;

    cout << "Which character you want to check for : ";
    cin >> check;
    string1.one_char(check);

    cout << "Which char you want to set : "; //set the character.
    cin >> set;
    cout << "Which place you want to set : "; //place the character.
    cin >> located;
    string1.set_char(set, located);

    cout << "Where to start : ";
    cin >> begin;
    cout << "Where to stop : ";
    cin >> length; //the length of char.
    string1.copy_piece(begin, length);

    if (string1 == string2) {
     cout << "1st string is = to 2nd string" << endl;
    }
    else {
     cout << "1st string is != to 2nd string" << endl;
    }

    cout << "1st string + 2nd string is " << string1 + string2 << endl;
    
    return 0;
}

