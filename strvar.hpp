#ifndef STRVAR_H
#define STRVAR_H
#include <iostream>
using namespace std;
namespace strvarken
{
    class StringVar
    {
    public:
        StringVar(int size);// function of size of string.
        StringVar(); //default of size.
        StringVar(const char a[]);//use array a contains characters as value.
        StringVar(const StringVar& string_object);//copy
        ~StringVar();//use to return value.
        int length() const;//return lenght
        void input_line(istream& ins);
        friend ostream& operator <<(ostream& outs, const StringVar& the_string);
        void one_char(char find);
        void set_char(char set, int place);
        void copy_piece(int begin, int length);
        friend bool operator == (const StringVar& string1, const StringVar& string2);
        friend istream& operator >> (istream& ins, StringVar& string);
        friend StringVar operator + (const StringVar& string1, const StringVar& string2);
        

    private:
        char *value; //pointer to dynamic array
        int max_length; //maximum length of the string.
    };
}
#endif 
