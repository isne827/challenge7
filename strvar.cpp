#include <iostream>
#include <cstdlib>
#include <cstddef>
#include <cstring>
#include "strvar.hpp"

using namespace std;

namespace strvarken
{
    StringVar::StringVar(int size) : max_length(size) //calculated the size
    {
        value = new char[max_length + 1];
        value[0] = '\0';
    }

    
    StringVar::StringVar() : max_length(100) //calculated the length
    {
        value = new char[max_length + 1];
        value[0] = '\0';
    }

        StringVar::StringVar(const char a[]) : max_length(strlen(a))
    {
        value = new char[max_length + 1];

        for (int i = 0; i<strlen(a); i++)
        {
            value[i] = a[i];
        }
        value[strlen(a)] = '\0';
    }

    StringVar::StringVar(const StringVar& string_object) : max_length(string_object.length())
    {
        value = new char[max_length + 1];
        for (int i = 0; i<strlen(string_object.value); i++)
        {
            value[i] = string_object.value[i];
        }
        value[strlen(string_object.value)] = '\0';
    }

    StringVar::~StringVar()
    {
        delete[] value;
    }

    
    int StringVar::length() const
    {
        return strlen(value);
    }

    
    void StringVar::input_line(istream& ins)
    {
        ins.getline(value, max_length + 1);
    }

    void StringVar::one_char(char find){
        for(int i=0; i<strlen(value); i++){
            if(value[i] == find){
                cout << find << " is found at " << i << endl;
            }
        }
    }

    void StringVar::set_char(char set, int place){
        cout << "Before : " << value[place-1] << endl;
        value[place-1] = set; // input to the index that you want.
        cout << "After : " << value[place-1] << endl;
    }

    void StringVar::copy_piece(int start, int length){
        for(int i=start; i<start+length; i++){
            cout << value[i-1];
        }
        cout << endl;
    }

    //Uses iostream
    ostream& operator << (ostream& outs, const StringVar& the_string)
    {
        outs << the_string.value;
        return outs;
    }

    istream& operator >> (istream& ins, StringVar& string){
        ins >> string.value;
        return ins;
    }

    bool operator == (const StringVar& string1, const StringVar& string2){
        if(strcmp(string1.value, string2.value) == 0){
            return true;
        }
        return false;
    }
StringVar operator + (const StringVar& string1, const StringVar& string2){
    StringVar x;
    int index = strlen(string1.value);
    for(int i=0; i<index; i++){
        x.value[i] = string1.value[i];
    }
    for(int i=index; i<strlen(string2.value)+index; i++){
        x.value[i] = string2.value[i-index];
    }
    return x;
}

}
